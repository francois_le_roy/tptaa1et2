package fr.istic.mil.tptaa1.dao;

import fr.istic.mil.tptaa1.model.KanbanTable;

public class KanbanTableDao extends AbstractJpaDao<Long, KanbanTable> {

	public KanbanTableDao() {
		super(KanbanTable.class);
	}

}
