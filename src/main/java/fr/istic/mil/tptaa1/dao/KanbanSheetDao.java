package fr.istic.mil.tptaa1.dao;

import fr.istic.mil.tptaa1.model.KanbanSheet;

public class KanbanSheetDao extends AbstractJpaDao<Long, KanbanSheet> {

	public KanbanSheetDao() {
		super(KanbanSheet.class);
		// TODO Auto-generated constructor stub
	}

}
