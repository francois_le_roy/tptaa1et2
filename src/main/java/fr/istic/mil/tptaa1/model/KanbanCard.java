package fr.istic.mil.tptaa1.model;

import java.io.Serializable;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;

import lombok.Data;

@Data
@Entity
public class KanbanCard implements Serializable {
	@Id
	@GeneratedValue
	private long id;
	@OneToOne(cascade = CascadeType.PERSIST)
	private KanbanSheet sheet;
	private int position;
	private int positionInTable;
}
