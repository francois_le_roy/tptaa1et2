package fr.istic.mil.tptaa1.model;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

import lombok.Data;

@Data
@Entity
public class Tag implements Serializable {
	@Id
	@GeneratedValue
	private long id;
	private String tag;
	public Tag() {}
	public Tag(String tag) {
		this.tag = tag;
	}
}
