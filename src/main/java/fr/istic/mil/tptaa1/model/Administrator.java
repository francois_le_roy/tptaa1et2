package fr.istic.mil.tptaa1.model;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

import lombok.Data;

@Data
@Entity
@DiscriminatorValue(value = "Admin")
public class Administrator extends User {

}
