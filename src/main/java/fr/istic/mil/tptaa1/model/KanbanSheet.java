package fr.istic.mil.tptaa1.model;

import java.io.Serializable;
import java.net.URL;
import java.util.Collection;
import java.sql.Date;
import java.util.List;

import javax.persistence.*;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import lombok.Data;

@Data
@Entity
public class KanbanSheet implements Serializable{
	@Id
	@GeneratedValue
	private Long id;
	private String wording;
	private Date dueDate;
	@ManyToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "userid")
	@JsonBackReference
	private User user;
	private int timeNeeded;
	@ManyToMany(cascade = CascadeType.PERSIST)
	private Collection<Tag> tags;
	private String location;
	private String url;
	private String note;
}
