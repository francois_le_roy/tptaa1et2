package fr.istic.mil.tptaa1.model;

import java.io.Serializable;
import java.util.Collection;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

import lombok.Data;

@Data
@Entity
public class KanbanTable implements Serializable {
	@Id
	@GeneratedValue
	private Long id;
	@OneToMany(targetEntity = KanbanCard.class,cascade = CascadeType.PERSIST)
	@JoinColumn(name="waitingid")
	private Collection<KanbanCard> waiting;
	@OneToMany(targetEntity = KanbanCard.class,cascade = CascadeType.PERSIST)
	@JoinColumn(name="inprogressid")
	private Collection<KanbanCard> inProgress;
	@OneToMany(targetEntity = KanbanCard.class,cascade = CascadeType.PERSIST)
	@JoinColumn(name="finishedid")
	private Collection<KanbanCard> finished;
}
