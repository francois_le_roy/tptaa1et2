package fr.istic.mil.tptaa1;

import java.util.HashSet;
import java.util.Set;

import javax.ws.rs.core.Application;

import fr.istic.mil.tptaa1.rest.KanbanTableRes;
import io.swagger.v3.jaxrs2.integration.resources.OpenApiResource;

public class TestApplication extends Application {


    @Override
    public Set<Class<?>> getClasses() {

        final Set<Class<?>> clazzes = new HashSet<Class<?>>();

        clazzes.add(KanbanTableRes.class);
        clazzes.add(OpenApiResource.class);


        return clazzes;
    }

}
