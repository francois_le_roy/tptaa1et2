package fr.istic.mil.tptaa1.rest;

import fr.istic.mil.tptaa1.dao.KanbanTableDao;
import fr.istic.mil.tptaa1.model.KanbanTable;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Response;
import io.swagger.v3.oas.annotations.Parameter;

@Path("/table")
@Produces({ "application/json", "application/xml" })
public class KanbanTableRes {
	KanbanTableDao dao = new KanbanTableDao();

	@GET
	@Path("/{tableId}")
	public KanbanTable getTableById(@PathParam("tableId") Long tableId) {
		return dao.findOne(tableId);
	}

	@POST
	@Consumes("application/json")
	public Response addPet(@Parameter(required = true) KanbanTable table) {
		dao.save(table);
		return Response.ok().entity("SUCCESS").build();
	}

}
