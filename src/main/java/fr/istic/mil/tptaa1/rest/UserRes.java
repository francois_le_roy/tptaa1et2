package fr.istic.mil.tptaa1.rest;

import fr.istic.mil.tptaa1.dao.UserDao;
import fr.istic.mil.tptaa1.model.User;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Response;
import io.swagger.v3.oas.annotations.Parameter;

@Path("/user")
@Produces({ "application/json", "application/xml" })
public class UserRes {
	UserDao dao = new UserDao();

	@GET
	@Path("/{userId}")
	public User getuserById(@PathParam("userId") Long userId) {
		return dao.findOne(userId);
	}

	@POST
	@Consumes("application/json")
	public Response addPet(@Parameter(required = true) User user) {
		dao.save(user);
		return Response.ok().entity("SUCCESS").build();
	}
}
