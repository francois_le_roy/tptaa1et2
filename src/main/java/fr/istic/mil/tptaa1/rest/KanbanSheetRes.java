package fr.istic.mil.tptaa1.rest;

import fr.istic.mil.tptaa1.dao.KanbanCardDao;
import fr.istic.mil.tptaa1.dao.KanbanSheetDao;
import fr.istic.mil.tptaa1.model.KanbanCard;
import fr.istic.mil.tptaa1.model.KanbanSheet;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Response;
import io.swagger.v3.oas.annotations.Parameter;

@Path("/sheet")
@Produces({ "application/json", "application/xml" })
public class KanbanSheetRes {
	KanbanSheetDao dao = new KanbanSheetDao();

	@GET
	@Path("/{sheetId}")
	public KanbanSheet getCardById(@PathParam("sheetId") Long sheetId) {
		return dao.findOne(sheetId);
	}

	@POST
	@Consumes("application/json")
	public Response addPet(@Parameter(required = true) KanbanSheet sheet) {
		dao.save(sheet);
		return Response.ok().entity("SUCCESS").build();
	}

}
