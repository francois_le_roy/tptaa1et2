package fr.istic.mil.tptaa1.rest;

import fr.istic.mil.tptaa1.dao.TagDao;
import fr.istic.mil.tptaa1.model.Tag;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Response;
import io.swagger.v3.oas.annotations.Parameter;

@Path("/tag")
@Produces({ "application/json", "application/xml" })
public class TagRes {
	TagDao dao = new TagDao();

	@GET
	@Path("/{tagId}")
	public Tag getTagById(@PathParam("tagId") Long tagId) {
		return dao.findOne(tagId);
	}

	@POST
	@Consumes("application/json")
	public Response addPet(@Parameter(required = true) Tag tag) {
		dao.save(tag);
		return Response.ok().entity("SUCCESS").build();
	}

}
