package fr.istic.mil.tptaa1.rest;

import fr.istic.mil.tptaa1.dao.KanbanCardDao;
import fr.istic.mil.tptaa1.model.KanbanCard;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Response;
import io.swagger.v3.oas.annotations.Parameter;

@Path("/card")
@Produces({ "application/json", "application/xml" })
public class KanbanCardRes {
	KanbanCardDao dao = new KanbanCardDao();

	@GET
	@Path("/{sheetId}")
	public KanbanCard getCardById(@PathParam("cardId") Long cardId) {
		return dao.findOne(cardId);
	}

	@POST
	@Consumes("application/json")
	public Response addPet(@Parameter(required = true) KanbanCard card) {
		dao.save(card);
		return Response.ok().entity("SUCCESS").build();
	}

}
