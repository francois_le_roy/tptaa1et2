package fr.istic.mil.tptaa1;

import java.net.URL;
import java.sql.Date;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;

import fr.istic.mil.tptaa1.dao.KanbanTableDao;
import fr.istic.mil.tptaa1.model.*;

public class JpaTest {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
			KanbanTableDao kbTableDao = new KanbanTableDao();
			User user1 = (User) new Developper();
			user1.setFirstName("francois");
			user1.setLastName("leroy");
			KanbanSheet sheet1 = new KanbanSheet(); 	
			sheet1.setWording("sheet1");
			sheet1.setLocation("location");
			sheet1.setNote("fiche1");
			sheet1.setTags(Arrays.asList(new Tag("fiche")));
			sheet1.setDueDate(new Date(0));
			sheet1.setTimeNeeded(0);
			sheet1.setUrl("http://fanieofnoeanf.fr");
			sheet1.setUser(user1);
			KanbanCard card1 = new KanbanCard();
			card1.setSheet(sheet1);
			card1.setPosition(0);
			card1.setPositionInTable(0);
			KanbanTable table = new KanbanTable();
			table.setWaiting(Arrays.asList(card1));
			table.setFinished(new ArrayList());
			table.setInProgress(new ArrayList());
			kbTableDao.save(table);
			System.out.println("RESULTS :: "+kbTableDao.findAll());
	}

}

