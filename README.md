## TP TAA 1 et 2
Il faut créer une table jpaTest et changer les identifiants de mysql dans persistence.xml.
```
create database testjpa;
```

Dans le package fr/istic/mil/tptaa1 la classe JpaTest permet de tester le modèle du TP1.
Elle instancie un Dao et les classe métier à persister.


La classe RestServer elle contient un main qui permet de lancer un serveur donnant accès à la ressource rest de la table.
Pour la tester, il faut envoyer du JSON avec POST et on peut ensuite effectuer un get.



Exemple de post :

```json
{
"waiting":[{
  "sheet":{
    "wording":"tache3",
    "dueDate":"2012-04-23T18:25:43.511Z",
    "user":{
      "firstName":"francois",
      "lastName":"le roy"
    },
    "tags":["tache"],
    "location":"",
    "url":"",
    "note":""
  },
  "position":0,
  "positionInTable":1
}],
"inProgress":[{
   "sheet":{
        "wording":"tache2",
    "dueDate":"2012-04-23T18:25:43.511Z",
    "user":{
      "firstName":"francois",
      "lastName":"le roy"
    },
    "tags":["tache"],
    "location":"",
    "url":"",
    "note":""
   },
  "position":1,
  "positionInTable":2
}],
"finished":[{
   "sheet":{
        "wording":"tache1",
    "dueDate":"2012-04-23T18:25:43.511Z",
     "user":{
      "firstName":"francois",
      "lastName":"le roy"
    },
    "tags":["tache"],
    "location":"",
    "url":"",
    "note":""
   },
  "position":0,
  "positionInTable":3
}]
}
```
